#!/usr/bin/fish

set wg_status ' '
set wg (nmcli c show | grep wg | cut -d ' ' -f 0)
if [ $wg = 'wg0' ]
    set wg_status '🔒'
end

set connection (nmcli --fields IN-USE,SSID,BARS dev wifi | grep "*" | cut -c9- | tr -s ' ')
set ip (ip a | grep wlan0 | grep inet | cut -d' ' -f6 | cut -d'/' -f1)
set date_formatted (date "+%a %F %H:%M")
set battery_phone_status (cat /sys/class/power_supply/axp20x-battery/capacity)
set battery_keyboard_status (cat /sys/class/power_supply/ip5xxx-battery/capacity)
set disk_free (df -h | grep root | tr -s ' ' | cut -d' ' -f4)
set ram_free (free -h | grep Mem | tr -s ' ' | cut -d' ' -f4)

echo $connection $ip $wg_status 🔋$battery_phone_status% 🔋$battery_keyboard_status% 💾$disk_free 🗃$ram_free $date_formatted