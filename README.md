This is the Sway configuration I use on my Pinephone. The Pinephone has a hardware keyboard attached. It uses the fish shell. To use it, put the files in your `~/.config/sway/` and do on Alpine:

`$ doas apk add fish font-noto-emoji`

On Arch, that would be:

`$ sudo pacman -S fish noto-fonts-emoji`